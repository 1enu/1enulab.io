---
title: "The First Post"
date: 2024-03-09
description: "First post, talk about the creation."
coverAlt: "Computer terminal with cursor"
coverCaption: "Photo by [Gabriel Heinzer](https://unsplash.com/@6heinz3r) on [Unsplash](https://unsplash.com/photos/green-and-black-digital-device-xbEVM6oJ1Fs)"
  
categories: ["hugo"]
tags: ["themes", "design"]
---

Now the site was formless and void, and darkness was over the surface of the deep. And the cursor was hovering over the terminal. Then it typed:

<!--more-->

{{< lead >}}
“hugo”
{{< /lead >}}

And the site existed. It saw that the site was good, then the script terminates.

## Before it even started :hourglass_not_done:

There are some _prerequisites_...

### Static website framework

There are quite a lot of framework such as Jekyll(from [github-pages](https://pages.github.com/) ), Nikola([pcg-random](https://www.pcg-random.org/) uses it), and others.

**What I chose:** [hugo](https://gohugo.io/)

Simply because it is the [fastest](https://css-tricks.com/comparing-static-site-generator-build-times/), and to interact with go, even if its just a very very slight brush on the language.

### Static website hosting

As with the first, there is a plethora of static file hosting such as Github Pages, Netlify and Vercel.

**What I chose:** [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)

Since I already made a GitLab account, why not try using its pages feature too?

### Website theme

Ah yes, the theme. Here's what i am looking for a good theme, by order:

- Has a demo site
- No other dependencies
- Light/Dark mode
- LaTeX support
- Search bar
- Good looking for my taste

As of right now, after clicking themes that looks good on [hugo themes](https://themes.gohugo.io/), I have refined the results to seven potential candidates, by the order of when I found them:

| Theme                                                                      | Demo                                               |
| -------------------------------------------------------------------------- | -------------------------------------------------- |
| [FeelIt](https://themes.gohugo.io/themes/feelit/) (needs npm)              | [Link](https://feelit.khusika.dev/)                |
| [Lightbi](https://themes.gohugo.io/themes/lightbi-hugo/)                   | [Link](https://lightbi-hugo-theme.netlify.app/)    |
| [Perplex](https://themes.gohugo.io/themes/perplex/) (search needs npm)     | [Link](https://themes.gohugo.io/themes/perplex/)   |
| [Hextra](https://themes.gohugo.io/themes/hextra/)                          | [Link](https://imfing.github.io/hextra/)           |
| [Tailwind](https://themes.gohugo.io/themes/hugo-theme-tailwind/)           | [Link](https://hugo-theme-tailwind.tomo.dev/)      |
| [Gruvbox](https://themes.gohugo.io/themes/hugo-theme-gruvbox/) (needs npm) | [Link](https://hugo-theme-gruvbox.schnerring.net/) |
| [Doks](https://themes.gohugo.io/themes/doks/) (needs npm)                  | [Link](https://doks.netlify.app/)                  |

**What I chose:** None

{{< lead >}}
What....
{{< /lead >}}
{{< lead >}}
Why are these even listed!?
{{< /lead >}}

Since I didn't use any of those, why not give it to others?

### Website theme—round two

I felt that because of clicking the themes that I think looks good, I didn't _really_ viewed all the other themes and may have missed _"That one theme that will definitely fit my needs"_ so I decided to click through **all 195(as of this time) themes under `blog` category**. Excluding the candidates above, this has produced six more candidates by order of discovery:

| Theme                                                                               | Demo                                                   |
| ----------------------------------------------------------------------------------- | ------------------------------------------------------ |
| [PaperMod](https://themes.gohugo.io/themes/hugo-papermod/)                          | [Link](https://adityatelange.github.io/hugo-PaperMod/) |
| [Congo](https://themes.gohugo.io/themes/congo/)                                     | [Link](https://jpanther.github.io/congo/)              |
| [Clarity](https://themes.gohugo.io/themes/hugo-clarity/)                            | [Link](https://neonmirrors.net/)                       |
| [Hugo Ficurinia](https://themes.gohugo.io/themes/hugo-ficurinia/) (only dark theme) | [Link](https://gabmus.org/)                            |
| [Beautiful Hugo](https://themes.gohugo.io/themes/beautifulhugo/) (no search)        | [Link](https://themes.gohugo.io/themes/beautifulhugo/) |
| [Arberia](https://themes.gohugo.io/themes/arberia/) (only light theme)              | [Link](https://arberiatheme.netlify.app/)              |

## Trying out themes :artist_palette:

With no more reason to defer choosing a theme, I need to actually choose which theme to try out. Here are the themes I tested.

### Perplex

![LEGO Bruce Wayne Zoom-In GIF](https://i.imgur.com/EpN91My.gif "It looks good")
The theme looks very good that it deserves an honorable mention. Its layout is unique, objects inside feels so free. I personally would've used it if it had an integrated search (needs npm for search feature). I am not against using npm, but I just want a simple website theme without requiring another tool.

- [ ] No dependencies
- [x] Has picture in its blog list

### Hextra

Not as expressive as Perplex in terms of layout, its design is still looks good. The only thing that prevented me from using it was that it didn't have the option to add pictures on its blog list. I could've edited its `layout` but I would need to know how to create a custom list html in hugo templates.

- [x] No dependencies
- [ ] Has picture in its blog list

### Congo

![The three bears goldilocks principle](https://upload.wikimedia.org/wikipedia/commons/e/ef/The_Three_Bears_-_Project_Gutenberg_eText_17034.jpg "Photo by[Arthur Rackham](http://www.gutenberg.org/etext/17034) found on [Wikipedia](https://commons.wikimedia.org/wiki/File:The_Three_Bears_-_Project_Gutenberg_eText_17034.jpg)")

- [x] No dependencies
- [x] Has picture in its blog list

{{< lead >}}
It is just right
{{< /lead >}}

The most important part is that it has picture in its blog list. Even if I don't agree with all of its style choices, with hugo's custom templates it should be much easier to remedy than changing Perplex's search module or changing Hextra's layout.

**What I chose:** [Congo](https://themes.gohugo.io/themes/congo/)

It has all the features that I need, it just needs some tweaks.

> We’ve all been there. You’re browsing through the Hugo themes and find one that you really like. It meets all your criteria, except for that one small thing.<br>
> — <cite>Zachary Wade Betz[^1]</cite>

[^1]: The above quote is found from [LoopedLine's blog](https://loopedline.com/post/changing-a-hugo-theme-over-time/).

## Customizing the theme :paintbrush:

Here are the following tweaks I have made to the theme, many of which is related to the default theme having a preference to things being in the left, as opposed to being centered.

### CSS

Note: I am not an expert in CSS, I just did some readings on [CSS tricks](https://css-tricks.com/) or congo's default `assets/css/compiled/main.css` till it looked good.

For the theme, I use an edited `avocado` color scheme(I replaced its neutral colors with `congo` theme's neutral colors) put into `assets/css/schemes/avocado-edited.css`

I also made a custom CSS for my needs. [Here](https://gitlab.com/1enu/congo-custom-template/-/blob/main/assets/css/custom.css) is the entire `assets/css/custom.css` file, the comments should provide an adequate explanation.

### Layouts

Mostly subtle edits on the original layout. Note that I am not an expert in html, I just tweaked using <kbd>F12</kbd> and used what worked.

#### \_default/baseof.html

Changed a single letter on `baseof.html`

{{< highlight html "linenos=table,linenostart=13" >}}

    class="m-auto flex h-screen max-w-7xl flex-col ....

{{< /highlight >}}

{{< highlight html "linenos=table,hl_lines=1,linenostart=13" >}}

    class="m-auto flex v-screen max-w-7xl flex-col ....

{{< /highlight >}}

Possibly related to [this issue](https://github.com/jpanther/congo/issues/796). This fixes that unusual cut in dark reader. I don't know the other consequences of this fix nor deep knowledge of what I just changed, but it did remove that cut.

#### \_default/single.html

There are multiple changes on this one, but I will try my best to explain what it does.

{{< highlight html "linenos=table,linenostart=6" >}}

    <header class="max-w-prose">

{{< /highlight >}}

{{< highlight html "linenos=table,hl_lines=1,linenostart=6" >}}

    <header class="max-w-full">

{{< /highlight >}}

I want the feature photo to be centered, to do this I need the header class to fully expand.

{{< highlight html "linenos=table,linenostart=25" >}}

        <div class="prose">
          {{ $altText := $.Params.featureAlt | default $.Params.coverAlt | default "" }}
          {{ $class := "mb-6 -mt-4 rounded-md" }}

{{< /highlight >}}

{{< highlight html "linenos=table,hl_lines=1 3,linenostart=25" >}}

        <div class="prose flex max-w-full flex-col">
          {{ $altText := $.Params.featureAlt | default $.Params.coverAlt | default "" }}
          {{ $class := "mx-auto mb-3 -mt-4 rounded-md" }}

{{< /highlight >}}

Changing the class from `prose` to `prose flex max-w-full flex-col` allows the image to expand if there is extra screen space, `flex-col` keeps the caption under the image. `mx-auto` was added to center the image, and `mb-3` is a reduced bottom margin.

{{< highlight html "linenos=table,linenostart=36" >}}

    <section class="prose mt-0 flex max-w-full flex-col dark:prose-invert lg:flex-row">
      {{ if and (.Params.showTableOfContents | default (.Site.Params.article.showTableOfContents | default false)) (in .TableOfContents "<ul") }}
        <div class="order-first px-0 lg:order-last lg:max-w-xs lg:ps-8">

{{< /highlight >}}

{{< highlight html "linenos=table,hl_lines=1 3,linenostart=36" >}}

    <section class="prose mt-0 flex max-w-full flex-col dark:prose-invert lg:flex-row justify-center">
      {{ if and (.Params.showTableOfContents | default (.Site.Params.article.showTableOfContents | default false)) (in .TableOfContents "<ul") }}
        <div class="order-first px-0 lg:order-last lg:max-w-xs lg:ps-8 toc-fix">

{{< /highlight >}}

You won't see the changes for line 36 here, as it only applies to pages with no table of contents, but it sets the content to the middle instead of being slightly left. For line 38 I added a `toc-fix` class in my `custom.css` to fix the table of contents being too tightly packed because of more content, which I also did in the css file.

#### shortcodes/alert.html

Not the best, but I made a custom info/warn/error alert shortcode based on the original one. [Here it is](https://gitlab.com/1enu/congo-custom-template/-/blob/7036422b851839877f2c3a512cc86f436a42f779/layouts/shortcodes/alert.html), do note that you need the colors from `custom.css` for it to properly work.

## Hugo init problems :spaghetti:

Remember how this site is about spaghet? Well, now it starts here. If you you copy your existing `hugo.yaml` to a new project directly after you created a new site:

```shell
hugo new site testing
cd testing
hugo mod get
```

it will result in an error:

```shell
Error: failed to load modules: module "github.com/jpanther/congo/v2" not found in "paths-here\\testing\\themes\\github.com\\jpanther\\congo\\v2"; either add it as a Hugo Module or store it in "paths-here\\testing\\themes".: module does not exist
```

It is also discussed [here](https://discourse.gohugo.io/t/hugo-latest-version-cant-support-server-command-under-examplesite-subfolder/34506/3), it may be from 2021 and the [issue is closed](https://github.com/gohugoio/hugo/issues/8940) but I still experienced it.
So, the steps I use to start a new project from an existing `hugo.yaml` is to:

1. Create a new site and run `hugo mod init` without using any config

```shell
hugo new site testing
cd testing
hugo mod init testing --config "nothing"
```

In the command, "nothing" is a nonexistent file, so I think hugo init works with the defaults. This is quite hacky, but it will properly initialize the project.

2. Now you can continue straight to `hugo server` and it will automatically get your modules from your `hugo.yaml`

## GitLab upload process :globe_with_meridians:

{{< lead >}}
The final stretch
{{< /lead >}}

### Workflow file

To make a GitLab page, you need to create a `.gitlab-ci.yml`. I just copied the [GitLab Hugo Template](https://docs.gitlab.com/ee/tutorials/hugo/#add-your-configuration-options), customized it, and used the technique above to init the site.

```yaml
default:
  image: "${CI_TEMPLATE_REGISTRY_HOST}/pages/hugo/hugo_extended:latest" # use hugo extended
  before_script: # init first before building
    - apk add --no-cache go
    - hugo mod init page --config "nothing"

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test: # builds and tests your site
  script:
    - hugo
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

pages: # a predefined job that builds your pages and saves them to the specified path.
  script:
    - hugo
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  environment: production

```

Now, the only step is to push the repo to gitlab.

### Pushing repo to GitLab from scratch

Pushing the local repo to gitlab without needing to create the project from the website only requires two commands:
```shell
git push -u git@gitlab.com:your_namespace/name_of_your_repo.git your_local_branch
```
My local branch is named "main", so in this case:
```shell
git push -u git@gitlab.com:1enu/1enu.gitlab.io.git main
```

After that, make sure to properly configure your remote as stated in the output:

```shell
git remote add origin git@gitlab.com:1enu/1enu.gitlab.io.git
```

The moment it is uploaded, the gitlab CI runs immediately, creating the site into existence! This will create a private project with the same name, you can change the visibility from `private` to `public` on **Settings > General > Visibility, project features, permissions**. After changing to `public`, make sure to scroll down and look for the "save changes" button to finalize it.

### Unique domain

When pushed, you will notice on your gitlab **Deploy > Pages** that your new site has the name

```
https://1enu-extremely_long_letters_and_numbers.gitlab.io/
```

To remove the random numbers, uncheck the **"Use unique domain"** option and save changes. Now the site is finally completed!

## Overhead of modules :straight_ruler:

Creating a site is one thing, another thing to talk about is *how big* the site is. [Here](https://radar.cloudflare.com/scan/4d11b48a-7654-4bc2-bd34-7d642f51be53/summary) is the congo docs with most of its available shortcode features used. More features means more data to load. And in this case, it is a whopping **3.7MB** uncompressed size. It sounds a lot, but if you look at the `Network` page you will see that Mermaid alone took **3.02MB**. With that, it leaves the rest of the website with around 700KB which is not the slimmest but it is much more palatable. As long as I use Mermaid only when absolutely needed, most pages will stay under 1MB before compression.

## What now? :chequered_flag:

Now that the site is complete, I can move to *\*actually doing cod\**. Like the design? Here is the [template](https://gitlab.com/1enu/congo-custom-template)—clone it, and customize it to your needs. More stuff will be added to the site, mainly about cod, spaghet, and more.
